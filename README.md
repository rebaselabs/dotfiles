# dot-files

Predefined dot files, can be sym-linked to the home directory

```
ln -s /path/to/original /path/to/symlink
```

example:

```
ln -s ~/Code/dotfiles/rubocop.yml ~/.rubocop.yml
```